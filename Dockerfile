FROM debian:jessie
# proxy
ENV http_proxy http://proxy:3128/
ENV https_proxy http://proxy:3128/
ENV no_proxy .esrf.fr,localhost
# update
RUN apt-get update 

#installs
RUN apt-get -y install python \
    gcc \
    g++ \
    libgsl0-dev \
    python-sip-dev \
    git \
    cmake \
    libjsoncpp-dev \
    libglib2.0-dev \
    libcbf-dev \
    libv4l-dev \
    curl \
    liblz4-dev \
    libzmq3-dev \
    libstdc++-4.9-dev \
    vim \
    python-numpy \
    bc \
    libssl-dev \
    libcfitsio-dev \
    libccfits-dev \
    libhdf5-dev \
    libconfig++-dev

#RUN apt-get update && apt-get -y install libflycapture2-dev

#sdks and other stuff
ADD limaneeds/ root/lib_for_lima
ADD .ssh/ root/.ssh/
ADD scripts/* root/
